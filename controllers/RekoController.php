<?php

namespace app\controllers;

use yii\httpclient\Client;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use app\models\AWS;
use app\models\RekognitionModel;
use yii\rest\ActiveController;


class RekoController extends ActiveController
{
    public $modelClass = 'app\models\RekognitionModel';

	public function behaviors()
	{
		return [
		    // ...
		  'contentNegotiator' => [
		    'class' => \yii\filters\ContentNegotiator::className(),
		    // 'only' => ['get'],
		    'formatParam' => '_format',
		    'formats' => [
		    'application/json' => \yii\web\Response::FORMAT_JSON,
		    ],
		  ],
		  'corsFilter' => [
		        'class' => \yii\filters\Cors::className(),
		    ],
		  'verbs' => [
		      'class' => \yii\filters\VerbFilter::className(),
		      'actions' => [
		          'get'  => ['post','get','options'],
		          'search' => ['post','options'],
		      ],
		  ]
		];
	}
	public function actionSearch(){
		if (isset($_POST['collection']) && isset($_POST['nameImg']) && isset($_POST['bucket'])) {
			$nameImg = $_POST['nameImg'];
			$bucket = $_POST['bucket'];
			$collection = $_POST['collection'];

			$reko = new RekognitionModel();
			$s3 = new AWS();

			$result = $s3->objectExist($bucket,$nameImg);
			if ($result != false) {
				$result = $reko->searchByImage($collection,$bucket,$nameImg);
				foreach ($result['FaceMatches'] as $key => $value) {
					$fotos[] = array(
										'ExternalImageId' => $value['Face']['ExternalImageId'],
										'Similarity' => $value['Similarity']
								);
				}

				$json = array(
								'name' => 'OK',
								'message' => 'Success',
								'code' => 0,
								'status' => 200,
								'data' => $fotos
							);
			}else{
				$json = array(
								'name' => 'Not found',
								'message' => 'The requested resource was not found.',
								'code' => 404,
								'status' => 404,
								'data' => array()
 							);
			}

		}else{
				$json = array(
								'name' => 'Bad Request',
								'message' => 'Bad Request Was Sent.',
								'code' => 400,
								'status' => 400,
								'data' => array()
 							);
		}
		return $json;
	}
  //   public function actionSearch($nameImg,$bucket,$collection){
  //   	$reko = new RekognitionModel();
  //   	$s3 = new AWS();

		// $result = $s3->objectExist($bucket,$nameImg);

		// if ($result != false) {
		// 	$result = $reko->searchByImage($collection,$bucket,$nameImg);

	 //        foreach ($result['FaceMatches'] as $key => $value) {
	 //        $fotos[] = array('ExternalImageId' => $value['Face']['ExternalImageId'],
	 //                         'Similarity' => $value['Similarity']);
	 //        }

	 //        $json = array(
	 //                        'name'=>'OK',
	 //                        'message'=>'Success',
	 //                        'code'=>0,
	 //                        'status'=>200,
	 //                        'data' => $fotos
	 //                    );
		// }else{
		// 	$json = array( 
		// 					'name'=>'Not found',
		// 					'message'=>'The requested resource was not found.',
		// 					'code'=>404,
		// 					'status'=>404,
		// 					'data'=>array()
		// 				);
		// }
		// return $json;
  //   }
}








