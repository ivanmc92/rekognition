<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content" id="content">
    </div>
</div>
<script type="text/javascript">
var img_arr = JSON.parse( '<?php echo json_encode($img_arr) ?>' );
window.onload = function(){
    getImage(img_arr);
}
function getImage(img_arr){
        var content = "<div>";
        $.ajax({
          url: '<?php echo \Yii::$app->getUrlManager()->createUrl('site/ajaxpre') ?>',
          type: 'POST',
          data: { img : JSON.stringify(img_arr) },
          success: function(data) {
            result = JSON.parse(data);

            for (var i = 0; i < result.presigned_url.length; i++) {
                content += "<img src="+result.presigned_url[i]+" width='20%' title="+result.img[i]+" onclick='search(this);'>";
            }
            content += "</div>";
            document.getElementById('content').innerHTML = content;

          },
          error: function(xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
          }
        });
        document.getElementById("content").innerHTML = content;
}

function loading(){
    document.getElementById("content").innerHTML = "<div style='text-align:center;'><img  src='http://haagglobal.com/am-site/modules/Order/images/ajax-loader.gif'></div>";
}

function search(element){
    loading();
    var img = $(element).attr('title');
        $.ajax({
          url: '<?php echo \Yii::$app->getUrlManager()->createUrl('site/ajaxsearch') ?>',
          type: 'POST',
          // data: { img : img_arr[i][j],id:i,alt:j },
          data: { img },
          success: function(data) {
            result = JSON.parse(data);
            // console.log(result);
            var result_img = [];
            for (var i = 0; i < result.fotos.length; i++) {
                result_img.push(result.fotos[i].ExternalImageId);
            }
            getImage(result_img);
          },
          error: function(xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
          }
        });
}

</script>
