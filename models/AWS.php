<?php
namespace app\models;
use Yii;
use yii\base\Model;
use Aws\S3\S3Client;
class AWS extends Model
{
    private $aws;
    private $s3;

        function __construct(){
            $this->aws = Yii::$app->awssdk->getAwsSdk();
            $this->s3 = $this->aws->createS3();
        }

    function browse($bucket='',$prefix='') {
        $result = $this->s3->listObjects(['Bucket' => $bucket,"Prefix" => $prefix])->toArray();
        foreach ($result as $r) {
            if (is_array($r)) {
                if (array_key_exists('statusCode',$r)) {
                    //echo 'Effective URL: '.$r['effectiveUri'].'<br />';
                } else {
                  foreach ($r as $item) {
                    //echo $item['Key'].'<br />';
                  }
                }
            } else {
              //echo $r.'<br />';
            }
        }
        return $result;
    }
    public function download($bucket='',$key ='') {
        $file = $this->s3->getObject([
            'Bucket' => $bucket,
            'Key' => $key,
        ]);
        return $file;
        // save it to disk
    }

    public function presignedRequest($bucket='',$key=''){
        $cmd =  $this->s3->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key' => $key
        ]);
        $request = $this->s3->createPresignedRequest($cmd, '+120 seconds');
        $presignedUrl = (string)$request->getUri();
        return $presignedUrl;
    }

    public function objectExist($bucket,$nameImg){
        $response = $this->s3->doesObjectExist($bucket, $nameImg);
        return $response;
    }
}




