<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/test_db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'en-US',
    'components' => [
        'db' => $db,
        'mailer' => [
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        // 'urlManager' => [
        //     'showScriptName' => true,
        // ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => 
                    // [
                    //     'POST reko/<nameImg>' => 'reko/search'
                    // ]
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => 'reko',
                    //'extraPatterns' => ['post/<nameImg:\d+>' => 'search',],
                    //array('reko/search', 'pattern'=>'reko/<nameImg:\w+>', 'verb'=>'POST'),
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        // 'request' => [
        //     'cookieValidationKey' => 'test',
        //     'enableCsrfValidation' => false,
        // ],
    ],
    'params' => $params,
];
