<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\httpclient\Client;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use app\models\AWS;
use app\models\RekognitionModel;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   
        $result = new \DirectoryIterator(dirname('/Applications/MAMP/htdocs/Ventura/bucketBase/bucket'));
            foreach ($result as $fileinfo) {
                if (!$fileinfo->isDot()) {
                  if ($fileinfo->getFilename() == '.DS_Store') {
                  }else{
                    $img_arr[] = $fileinfo->getFilename();

                }
            }
        }
        return $this->render('index',array('img_arr' => $img_arr));
    }
    function actionAjaxpre(){
        $s = new AWS();
    if(Yii::$app->request->post('img') != null ){
        $array=json_decode(Yii::$app->request->post('img'));
        // echo "<pre>";
        // print_r($array);
        // echo "</pre>";
        
        // $f['presigned_url'] = $s->presignedRequest('snappc',"proyectos/desarrollo/locacion1/1/1/08102019/1.JPG");

        foreach ($array as $key => $value) {
            $f['presigned_url'][] = $s->presignedRequest('btestd',$value);
            $f['img'][] = $value;
        }
    }else{
        $test = "Ajax failed";
    }
    return \yii\helpers\Json::encode($f);
    }

    function actionAjaxsearch(){
        if (Yii::$app->request->post('img') != null) {
            $reko = new RekognitionModel();
            $result = $reko->searchByImage(Yii::$app->request->post('img'));

                // echo "<pre>";
                //     print_r($result['FaceMatches']);
                // echo "</pre>";
            foreach ($result['FaceMatches'] as $key => $value) {

            $fotos[] = array('ExternalImageId' => $value['Face']['ExternalImageId'],
                             'FaceId' => $value['Face']['FaceId']);
            }

            $json = array(
                            'ruta'=>'ruta',
                            'fotos' => $fotos
                        );
        }
        return \yii\helpers\Json::encode($json);
        //return $this->render('result',array('result' => \yii\helpers\Json::encode($json)));
    }
    function actonSearch(){
        //echo "perro";
        // if (Yii::$app->request->post('img') != null) {
        //     print_r(Yii::$app->request->post('img'));
        //     $reko = new RekognitionModel();
        //     $result = $reko->searchByImage(Yii::$app->request->post('img'));

        //         // echo "<pre>";
        //         //     print_r($result['FaceMatches']);
        //         // echo "</pre>";
        //     foreach ($result['FaceMatches'] as $key => $value) {

        //     $fotos[] = array('ExternalImageId' => $value['Face']['ExternalImageId'],
        //                      'FaceId' => $value['Face']['FaceId']);
        //     }

        //     $json = array(
        //                     'ruta'=>'ruta',
        //                     'fotos' => $fotos
        //                 );
        //     //$json = \yii\helpers\Json::encode($json);
        // }
        // return $this->render('result',array('result' => $json));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
