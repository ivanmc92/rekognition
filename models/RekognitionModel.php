<?php
namespace app\models;
use Yii;
use yii\base\Model;
use Aws\Rekognition\RekognitionClient;
use Aws\S3\Exception\S3Exception;

class RekognitionModel extends Model
{
    private $aws;
    private $rekognition;

        function __construct(){
            $this->aws = Yii::$app->awssdk->getAwsSdk();
            $this->rekognition = $this->aws->createRekognition();
        }

    function searchByImage($collection,$bucket,$img){
        $result = $this->rekognition->searchFacesByImage([
            'CollectionId' => $collection, // REQUERIDO
            'FaceMatchThreshold' => 90.0,
            'Image' => [// REQUERIDO
                'S3Object' => [
                    'Bucket' => $bucket,
                    'Name' => $img
                ],
            ],
            'MaxFaces' => 100,
        ]);
        return $result;
}
    
}